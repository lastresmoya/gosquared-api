'use strict';
const express = require('express');
const serverless = require('serverless-http');
const app = express();
const bodyParser = require('body-parser');

const router = express.Router();



app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(bodyParser.json());
app.use('/.netlify/functions/api', router);  // path must route to lambda


router.get('/', (req, res) => {
  
  res.writeHead(200, { 'Content-Type': 'application/json' });
  var json = JSON.stringify({
    visitors: 256,
    officeTempCelsius: 23,
    nextPlantWatering: "3:00pm",
    weather: {
      forecast: 'Showers',
      humidityPercentage: 72,
      windKmH: 9
    },
    nextHoliday: 'Sept 19th',
    drinksLeft: {
      cokeZero: 2,
      orangeJuice: 1,
      sprite: 0
    },
    nextBDay: {
      name: 'James Gill',
      date: 'Today',
      email: 'j.gill@gosquared.com',
      pic: 'https://angel.co/cdn-cgi/image/width=120,height=120,format=auto,fit=cover/https://d1qb2nb5cznatu.cloudfront.net/users/45998-original?1486140352'
    }

  });
  res.end(json);
});
router.get('/another', (req, res) => res.json({ route: req.originalUrl }));
router.post('/', (req, res) => res.json({ postBody: req.body }));


module.exports = app;
module.exports.handler = serverless(app);